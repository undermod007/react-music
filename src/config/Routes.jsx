import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from '../pages/Home';
import Login from '../pages/Login';
import Callback from '../pages/Callback';
import Album from '../pages/Album';

const Routes = () => {
    return (
        <Switch>
            <Route
                path='/'
                exact
                component={Home}
            />
            <Route
                path='/login'
                component={Login}
            />
            <Route
                path='/callback'
                component={Callback}
            />
            <Route
                path='/album/:id'
                component={Album}
            />
        </Switch>
    );
};

export default Routes;