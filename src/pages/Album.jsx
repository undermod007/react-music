import { Box } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import spotifyApi from '../components/api/spotifyApi';
import { useParams } from 'react-router';
import AlbumHead from '../components/AlbumHead';
import TrackLists from '../components/TrackLists';

const Album = () => {

    const { id } = useParams();
    const [album, setAlbum] = useState([]);

    console.log(id)

    useEffect(() => {
        const album = spotifyApi.getAlbum(id);
        album.then((res) => {
            // console.log(res.body);
            setAlbum(res.body);
        }).catch((error) => {
            const refreshedToken = spotifyApi.getRefreshToken();
            refreshedToken.then((res) => {
                // console.log(res);
                localStorage.setItem('access_token', res.data.access_token);
            })
        });
    }, []);

    return (
        <Box>
            <Box bgGradient='linear(to-l, #7928CA, #FF0080)' p={{base: '2', md: '5'}} rounded={'lg'}>
                <AlbumHead data={album}/>
            </Box>
            <TrackLists data={album}/>
        </Box>
    );
};

export default Album;