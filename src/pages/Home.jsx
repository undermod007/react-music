import { Heading } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import spotifyApi from '../components/api/spotifyApi';
import NewRelease from '../components/NewRelease';

const Home = () => {

    const [newReleases, setNewReleases] = useState([]);

    useEffect(() => {
        const getNewReleases = spotifyApi.getNewReleases();
        getNewReleases.then((res) => {
            // console.log(res.body.albums);
            setNewReleases(res.body.albums);
        }).catch((error) => {
            const refreshedToken = spotifyApi.getRefreshToken();
            refreshedToken.then((res) => {
                // console.log(res);
                localStorage.setItem('access_token', res.data.access_token);
            })
        })
    }, [])

    return (
        <div>
            <Heading
                size='md'
            >
                New Release
            </Heading>
            <NewRelease data={newReleases}/>
        </div>
    );
};

export default Home;