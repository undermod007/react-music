import React from 'react';
import {
    Box,
    Flex,
    Button,
    Link
} from '@chakra-ui/react';
import { ColorModeSwitcher } from '../ColorModeSwitcher';
import QueryString from 'qs';

const Login = () => {

    // const scopes = [
    //     'user-read-playback-position,user-read-email,playlist-modify-private,playlist-read-private,user-library-modify,playlist-read-collaborative,user-follow-read,user-read-playback-state,user-read-recently-played,user-read-private,playlist-modify-public,playlist-read-public,user-library-read,user-top-read,ugc-image-upload,user-follow-modify,user-read-currently-playing'
    // ];
    const scopes = [
        "user-read-email",
        "playlist-read-private",
        "playlist-read-collaborative",
        "user-read-email",
        "streaming",
        "user-read-private",
        "user-library-read",
        "user-top-read",
        // "user-library-modify"
        "user-read-playback-state",
        "user-modify-playback-state",
        "user-read-currently-playing",
        "user-read-recently-played",
        "user-read-playback-state",
        "user-follow-read",
    ].join(",");
    const redirectUri = 'https://react-music-staging.vercel.app/callback';
    const clientId = '29a127016d094fad9af61debcd1c02fb'

    const handleClick = () => {
        window.location.href = 'https://accounts.spotify.com/authorize?' + QueryString.stringify({
            response_type: 'code',
            client_id: clientId,
            scope: scopes,
            redirect_uri: redirectUri,
        });
    }

    return (
        <Box>
            <Flex
                w='full'
                h={{
                    base: '94vh', md: '91.4vh'
                }}
                p='5'
                justify='center'
                align='center'
            >
                <Button
                    colorScheme='teal'
                    rounded='full'
                    w='500px'
                    textTransform='uppercase'
                    fontWeight='900'
                    boxShadow='lg'
                    onClick={() => handleClick()}
                >
                    Sign In With Spotify
                </Button>
                <ColorModeSwitcher/>
            </Flex>
            <Box
                w='full'
                p='3'
                textAlign='center'
            >
                <Link
                    fontWeight='900'
                    href='https://developer.spotify.com'
                    target='_blank'
                >
                    Spotify API
                </Link>
            </Box>
        </Box>
    );
};

export default Login;