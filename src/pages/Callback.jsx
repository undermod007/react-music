import React, { useEffect } from 'react';
import {
    Box,
    Flex,
    Button,
    Link
} from '@chakra-ui/react';
import { ColorModeSwitcher } from '../ColorModeSwitcher';
import { Buffer } from 'buffer';
import QueryString from 'qs';
import axios from 'axios';

const Callback = () => {

    const token = new Buffer('29a127016d094fad9af61debcd1c02fb:426716a50b2b4aa69a52fc654a61a10b').toString('base64');
    const code = new URLSearchParams(window.location.search).get('code');
    const data = QueryString.stringify({
        code: code,
        redirect_uri: 'https://react-music-staging.vercel.app/callback',
        grant_type: 'authorization_code',
    });

    const reqAccessToken =  async () => {
        const res = await axios.post('https://accounts.spotify.com/api/token', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${token}`
            }
        }).then((res) => {
            const accessToken = res.data;
            localStorage.setItem('access_token', accessToken.access_token)
            localStorage.setItem('refresh_token', accessToken.refresh_token)
            console.log(accessToken);
        }).then(() => {
            const token = localStorage.getItem('access_token');
            if(token) {
                window.location.replace('/');
            }
        }).catch((error) => {
            console.log(error);
        })
    }

    useEffect(() => {
        reqAccessToken();
    }, []);

    return (
        <Box>
            <Flex
                w='full'
                h={{
                    base: '94vh', md: '91.4vh'
                }}
                p='5'
                justify='center'
                align='center'
            >
                <Button
                    colorScheme='teal'
                    rounded='full'
                    w='500px'
                    textTransform='uppercase'
                    fontWeight='900'
                    boxShadow='lg'
                    isLoading
                >
                    Sign In With Spotify
                </Button>
                <ColorModeSwitcher/>
            </Flex>
            <Box
                w='full'
                p='3'
                textAlign='center'
            >
                <Link
                    fontWeight='900'
                    href='https://developer.spotify.com'
                    target='_blank'
                >
                    Spotify API
                </Link>
            </Box>
        </Box>
    );
};

export default Callback;