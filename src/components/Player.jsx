import { Box } from '@chakra-ui/react';
import React from 'react';

import SpotifyPlayer from 'react-spotify-web-playback';
import spotifyApi from './api/spotifyApi';

const Player = ({data}) => {

    return (
        <Box
            h={{base: '170px', md: '0'}}
        >
            <SpotifyPlayer
                token={spotifyApi.getAccessToken()}
                uris={[data]}
                autoPlay={true}
                syncExternalDevice={true}
            />
        </Box>
    );
};

export default Player;