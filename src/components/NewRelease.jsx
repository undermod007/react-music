import { Box } from '@chakra-ui/react';
import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import Card from './Card';

const NewRelease = ({data}) => {
    return (
        <Box>
            <Swiper
                grabCursor={true}
                slidesPerView={5}
                spaceBetween={10}
            >
                {
                    data.items && data.items.map((item) => {
                        return (
                            <SwiperSlide key={item.id}>
                                <Card data={item}/>
                            </SwiperSlide>
                        )
                    })
                }
            </Swiper>
        </Box>
    );
};

export default NewRelease;