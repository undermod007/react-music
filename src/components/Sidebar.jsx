import React, { useEffect, useState } from 'react';
import {
    useDisclosure,
    Box,
    useColorModeValue,
} from '@chakra-ui/react';
import SidebarContent from './SidebarContent';
import MobileNav from './MobileNav';
import {
    MdLibraryMusic,
    MdSearch
} from 'react-icons/md';
import {
    RiHomeHeartFill
} from 'react-icons/ri';
import Nav from './Nav';
import { useLocation } from 'react-router-dom';
import Login from '../pages/Login';
import Callback from '../pages/Callback';
import spotifyApi from './api/spotifyApi';

const LinkItems = [
    { name: 'Home', icon: RiHomeHeartFill },
    { name: 'Search', icon: MdSearch },
    { name: 'Library', icon: MdLibraryMusic },
]

const Sidebar = ({children}) => {

    const { isOpen, onOpen, onClose } = useDisclosure();
    const [user, setUser] = useState([]);
    
    const route = useLocation();

    const bg = useColorModeValue('gray.100', 'gray.900');

    useEffect(() => {
        if(route.pathname !== '/login' && route.pathname !== '/callback') {
            const getCurrentUser = spotifyApi.getCurrentUser();
            getCurrentUser.then((res) => {
                // console.log(res);
                setUser(res.body);
            }).catch((error) => {
                const refreshedToken = spotifyApi.getRefreshToken();
                refreshedToken.then((res) => {
                    // console.log(res);
                    localStorage.setItem('access_token', res.data.access_token);
                })
            })
        }
    }, []);

    if(route.pathname == '/login') {
        return (
            <Login/>
        )
    } else if(route.pathname == '/callback') {
        return (
            <Callback/>
        )
    }

    return (
        <Box minH="100vh" bg={bg}>
            <SidebarContent
                LinkItems={LinkItems}
                onClose={() => onClose}
                display={{ base: 'none', md: 'block' }}
            />
            {/* mobilenav */}
            <MobileNav user={user} onOpen={onOpen} />
            <Box ml={{ base: 0, md: 60 }} p="4" bg={bg}>
                {children}
            </Box>
            {/* Nav Link */}
            <Nav LinkItems={LinkItems}/>
        </Box>
    );
};

export default Sidebar;