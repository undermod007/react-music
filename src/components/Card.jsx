import React from 'react';
import {
    Box,
    Heading,
    Image,
    Link,
    Text,
    useColorModeValue
} from '@chakra-ui/react';

const Card = ({data}) => {

    console.log(data)

    return (
        <Box
            maxW={{base: '120px', md: '200px'}}
            p={{base: '1', md: '3'}}
            bg={useColorModeValue('white', 'gray.800')}
            rounded={'lg'}
            transition='all .3s ease-out'
        >
            <Link href={`/album/${data.id}`}>
                <Image
                    src={data && data.images[1].url}
                    alt={data && data.name}
                    rounded={'lg'}
                    boxShadow={{base: 'md', md: 'lg'}}
                />
            </Link>
            <Heading
                size={{base: 'xs', md: 'sm'}}
                mt={'3'}
                fontWeight={{base: '500', lg: '800'}}
            >
                <Link href={`/album/${data.id}`}>
                    {
                        data && data.name.length > 10 ? data.name.substring(0, 12) + '...' : data.name
                    }
                </Link>
            </Heading>
            <Text mt={'2'} fontSize={{base: 'xs', md: 'sm'}} color={useColorModeValue('blackAlpha.500', 'whiteAlpha.500')}>
            {
                data && data.artists.map((item) => {
                    return (
                            <Link href={`/artist/${item.id}`}>
                                {item.name}&nbsp;
                            </Link>
                        )
                    })
            }
            </Text>
        </Box>
    );
};

export default Card;