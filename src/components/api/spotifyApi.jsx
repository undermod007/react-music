import axios from 'axios';
import QueryString from 'qs';
import SpotifyWebApi from 'spotify-web-api-node';
import { Buffer } from 'buffer';

const spotify = new SpotifyWebApi({
    accessToken: localStorage.getItem('access_token'),
    refreshToken: localStorage.getItem('refresh_token')
});

const spotifyApi = {
    getCurrentUser: () => {
        const res = spotify.getMe();
        return res;
    },
    getRefreshToken: async () => {
        const token = new Buffer('29a127016d094fad9af61debcd1c02fb:426716a50b2b4aa69a52fc654a61a10b').toString('base64');
        const data = QueryString.stringify({
            grant_type: 'refresh_token',
            refresh_token: localStorage.getItem('refresh_token')
        });

        const res = await axios.post('https://accounts.spotify.com/api/token', data, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Basic ${token}`
            }
        });
        return res;
    },
    getNewReleases: () => {
        const res = spotify.getNewReleases();
        return res;
    },
    getAlbum: (id) => {
        const res = spotify.getAlbum(id);
        return res;
    },
    getTrackInfo: (id) => {
        const res = spotify.getTrack(id);
        return res;
    },
    getAccessToken: () => {
        const res = spotify.getAccessToken();
        return res;
    }
}

export default spotifyApi;