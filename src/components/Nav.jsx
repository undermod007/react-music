import React from 'react';
import {
    Flex,
    useColorModeValue
} from '@chakra-ui/react';
import NavItem from './NavItem';

const Nav = ({LinkItems, ...rest}) => {
    return (
        <Flex
            ml='0'
            px='4'
            height="20"
            alignItems="center"
            bg={useColorModeValue('white', 'black')}
            borderBottomWidth="1px"
            borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
            justifyContent='space-between'
            bottom='0'
            position='fixed'
            w='full'
            display={{base: 'flex', md: 'none'}}
            zIndex={'2'}
            {...rest}
        >
            {LinkItems.map((link) => (
                <NavItem key={link.name} icon={link.icon}>
                    {link.name}
                </NavItem>
            ))}
        </Flex>
    );
};

export default Nav;