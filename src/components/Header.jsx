import React from 'react';

import { ColorModeSwitcher } from '../ColorModeSwitcher';

import {
    Flex,
    Box,
    Spacer,
    ButtonGroup,
    Button,
    Heading,
    useColorModeValue
} from '@chakra-ui/react';

import { Link, useLocation } from 'react-router-dom';

const Header = () => {

    const location = useLocation();

    const borderColor = useColorModeValue('gray.200', 'gray.700');

    if(location.pathname == '/login' || location.pathname == '/callback') {
        return (
            <>
            </>
        )
    }

    return (
        <Box
            pt='3'
            pr='5'
            pb='3'
            pl='5'
            _hover={{
                boxShadow:'lg'
            }}
            transition='.3s ease-out'
            borderBottom='1px'
            borderColor={borderColor}
        >
            <Flex minWidth='max-content' alignItems='center' gap='2'>
                <Box>
                    <Heading size='md'>Spotify From Hell</Heading>
                </Box>
                <Spacer />
                <ButtonGroup gap='2'>
                    <Button
                        colorScheme='teal'
                        variant='ghost'
                    >
                        <Link to='/'>
                            Home
                        </Link>
                    </Button>
                    <Button
                        colorScheme='teal'
                        variant='ghost'
                    >
                        <Link to='/search'>
                            Search
                        </Link>
                    </Button>
                    <Button
                        colorScheme='teal'
                        variant='ghost'
                    >
                        <Link to='/library'>
                            Library
                        </Link>
                    </Button>
                    <ColorModeSwitcher justifySelf="flex-end" />
                </ButtonGroup>
            </Flex>
        </Box>
    );
};

export default Header;