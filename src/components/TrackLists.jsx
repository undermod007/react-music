import React, { useEffect, useState } from 'react';

import {
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td,
    TableContainer,
    Icon,
    Text,
    useColorModeValue,
    Box,
} from '@chakra-ui/react'

import {
    FiClock,
} from 'react-icons/fi';
import spotifyApi from './api/spotifyApi';
import Player from './Player';

const TrackLists = ({data}) => {

    const bg = useColorModeValue('white', 'gray.800');
    const [datas, setDatas] = useState([]);

    const duration = (millis) => {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }

    const handleSong = (id) => {
        // console.log(id)
        const track = spotifyApi.getTrackInfo(id);
        track.then((res) => {
            setDatas(res.body?.uri);
            console.log(datas);
        })
    }

    return (
        <Box>
            <TableContainer mt={'5'} rounded={'lg'}>
                <Table variant='unstyled'>
                        <Thead borderBottom={'1px'} borderBottomColor={useColorModeValue('blackAlpha.400', 'whiteAlpha.400')}>
                            <Tr>
                                <Th w={'0'}>#</Th>
                                <Th maxW={{base: '240px', md: 'full'}}>TITLE</Th>
                                <Th isNumeric textAlign={'right'}>
                                    <Icon as={FiClock} mr={'6'} w={'4'} h={'4'}/>
                                </Th>
                            </Tr>
                        </Thead>
                        <Tbody>
                            {
                                data.tracks && data.tracks.items.map((item) => {
                                    return (
                                        <Tr 
                                            key={item.id}
                                            sx={{
                                                pointer: 'cursor'
                                            }}
                                            _hover={{
                                                // bgClip: 'text',
                                                bgGradient: 'linear(to-l, #7928CA, #FF0080)',
                                            }}
                                            // _active={{
                                            //     bgClip: 'text',
                                            //     bgGradient: 'linear(to-l, #7928CA, #FF0080)'
                                            // }}
                                            onClick={() => handleSong(item.id)}
                                        >
                                            <Td>
                                                <Text position={'relative'}>
                                                    {item.track_number}
                                                </Text>
                                            </Td>
                                            <Td display={{base: 'none', md: 'table-cell'}} maxW={{base: '240px', md: 'full'}}>
                                                <Box display={'inline-block'}>
                                                    {item.name}
                                                </Box>
                                            </Td>
                                            {
                                                item.name.length > 17 ? 
                                                    <Td display={{base: 'table-cell', md: 'none'}} maxW={{base: '240px', md: 'full'}}>
                                                        <Box display={'inline-block'}>
                                                            {item.name.substring(0, 17)}...
                                                        </Box>
                                                    </Td>
                                                    :
                                                    <Td display={{base: 'table-cell', md: 'none'}} maxW={{base: '240px', md: 'full'}}>
                                                        <Box display={'inline-block'}>
                                                            {item.name}
                                                        </Box>
                                                    </Td>
                                            }
                                            <Td isNumeric textAlign={'right'}>
                                                <Text mr={'4'}>
                                                    {duration(item.duration_ms)}
                                                </Text>
                                            </Td>
                                        </Tr>
                                    )
                                })
                            }
                        </Tbody>
                </Table>
            </TableContainer>
            <Player data={datas}/>
        </Box>
    );
};

export default TrackLists;