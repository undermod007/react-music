import { Box, Flex, Heading, Image, Text, Link } from '@chakra-ui/react';
import React from 'react';

const AlbumHead = ({data}) => {

    console.log(data)

    return (
        <Flex
            direction={'row'}
            align={{base: 'center', md: 'flex-end'}}
        >
            <Image
                src={data.images && data.images[1].url}
                rounded={'lg'}
                boxShadow={'lg'}
                w={{base: '100px', md:'300px'}}
                h={{base: '100px', md: '300px'}}
            />
            <Box ml={'5'}>
                <Heading textTransform={'uppercase'} size={'xs'}>
                    {data && data.album_type}
                </Heading>
                <Heading size={{base: 'lg', md: '3xl'}}>
                    {data && data.name}
                </Heading>
                <Box fontSize={{base: 'xs', md: 'sm'}} mt={{base: '1', md: '5'}}>
                    {
                        data.artists && data.artists.map((item) => {
                            return (
                                <Link key={item.id} href={`/artist/${item.id}`} fontWeight={'700'}>
                                    {item.name}&nbsp;
                                </Link>
                            )
                        })
                    }
                    &bull;
                    &nbsp;
                    {data && data.release_date}
                    &nbsp;
                    &bull;
                    &nbsp;
                    {data && data.total_tracks > 1 ? data.total_tracks + ' songs' : data.total_tracks + ' song'}
                </Box>
            </Box>
        </Flex>
    );
};

export default AlbumHead;