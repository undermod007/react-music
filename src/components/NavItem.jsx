import React from 'react';
import {
    Link,
    Flex,
    Icon,
    Text
} from '@chakra-ui/react';

const NavItem = ({ icon, children, ...rest }) => {
    return (
        <Link href="#" style={{ textDecoration: 'none' }} _focus={{ boxShadow: 'none' }}>
            <Flex
                align="center"
                p="4"
                mx="4"
                borderRadius="lg"
                role="group"
                cursor="pointer"
                _hover={{
                    bg: 'cyan.400',
                    color: 'white',
                }}
                {...rest}
            >
                {icon && (
                    <Icon
                        mr={{base: 0, md: 4}}
                        fontSize={{base: '20', md: '19'}}
                        _groupHover={{
                            color: 'white',
                        }}
                        as={icon}
                    />
                )}
                    <Text
                        display={{base: 'none', md: 'block'}}
                    >
                        {children}
                    </Text>
            </Flex>
        </Link>
    );
};

export default NavItem;