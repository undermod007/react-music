import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Routes from './config/Routes';
import 'swiper/swiper.min.css';
import './styles/style.css';

const App = () => {
	return (
		<BrowserRouter>
			<Route render={props => (
				<Sidebar {...props}>
					<Routes/>
				</Sidebar>
			)}/>
		</BrowserRouter>
	);
};

export default App;